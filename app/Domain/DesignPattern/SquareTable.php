<?php

namespace App\Domain\DesignPattern;

class SquareTable extends ShapedTable
{
    private $length;

    public function __construct($length)
    {
        $this->length = $length;
        parent::__construct("SQUARE");
    }

    public function getArea(): float
    {
        return $this->length * $this->length;
    }
}
