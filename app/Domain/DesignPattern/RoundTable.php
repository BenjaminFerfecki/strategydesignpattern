<?php

namespace App\Domain\DesignPattern;

class RoundTable extends ShapedTable
{
    public $radius;

    public function __construct($radius)
    {
        $this->radius = $radius;
        parent::__construct("ROUND");
    }

    public function getArea(): float
    {
        return $this->radius * $this->radius * pi();
    }
}
