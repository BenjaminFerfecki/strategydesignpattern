<?php

namespace App\Domain\DesignPattern;

class TriangleTable extends ShapedTable
{
    public $length;
    public $width;

    /**
     * @param $length
     * @param $width
     */
    public function __construct($length, $width)
    {
        $this->length = $length;
        $this->width = $width;
        parent::__construct("TRIANGLE");
    }

    public function getArea(): float
    {
        return $this->length * $this->width * 0.5;
    }
}
