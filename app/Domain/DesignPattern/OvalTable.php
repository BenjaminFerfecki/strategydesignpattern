<?php

namespace App\Domain\DesignPattern;

class OvalTable extends ShapedTable
{
    public $length;
    public $width;

    public function __construct($length, $width)
    {
        $this->length = $length;
        $this->width = $width;
        parent::__construct("OVAL");
    }

    public function getArea(): float
    {
        return $this->length * $this->width * pi();
    }
}
