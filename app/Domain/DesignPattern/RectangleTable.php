<?php

namespace App\Domain\DesignPattern;

class RectangleTable extends ShapedTable
{
    public $length;
    public $width;

    public function __construct($length, $width)
    {
        $this->length = $length;
        $this->width = $width;
        parent::__construct("RECTANGLE");
    }

    public function getArea(): float
    {
        return $this->length * $this->width;
    }
}
