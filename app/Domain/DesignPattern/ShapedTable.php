<?php

namespace App\Domain\DesignPattern;

abstract class ShapedTable
{
    public $shape;
    public $price;
    public abstract function getArea():float;

    public function __construct($shape)
    {
        $this->shape = $shape;
        $this->price = number_format($this->calculatePrice(), 2);
    }

    private function calculatePrice(): float
    {
        return $this->getArea() * 0.3;
    }
}
